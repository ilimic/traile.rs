<?php
/**
 * The template for displaying all single posts.
 *
 * @package trailers
 */

get_header(); ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php
		$movie_meta = get_post_custom();
		?>
		<header class="trailerpage">
			<div class="trailerbg">
				<div id="trailerwrapper">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo esc_attr( $movie_meta['_trailer_youtube_id'][0] ); ?>?rel=0" frameborder="0" allowfullscreen></iframe>
				</div>
				<div id="trailerinfo">
					<?php the_title( '<h1>', '</h1>' ); ?>
					<ul id="trailerinfo-tabs" class="">
						<li id="trailerinfo-tabs-facebook">
							<div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
						</li>
						<li id="trailerinfo-tabs-twitter">
							<a class="twitter-share-button" href="https://twitter.com/share" data-text="Watching <?php the_title( '', '' ); ?> on @traile.rs">Tweet</a>
							<script type="text/javascript">
								window.twttr=(function(d,s,id){var t,js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id)){return}js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);return window.twttr||(t={_e:[],ready:function(f){t._e.push(f)}})}(document,"script","twitter-wjs"));
							</script>
						</li>
					</ul>
					<div class="">
						<div class="" id="trailerinfo-description"><?php the_content(); ?></div>
					</div>
				</div>
			</div>
		</header>
	<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
