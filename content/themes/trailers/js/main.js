jQuery( document ).ready(function($) {
	var mySwiper = new Swiper('header .swiper-container',{
		pagination: 'header .pagination',
		loop:true,
		grabCursor: true,
		paginationClickable: true
	})
	$('header .arrow-left').on('click', function(e){
	      e.preventDefault()
	      mySwiper.swipePrev()
	})
	$('header .arrow-right').on('click', function(e){
	      e.preventDefault()
	      mySwiper.swipeNext()
	})


	var featuredMoviesSwiper = new Swiper('#featured-movies .swiper-container',{
	      grabCursor: true,
	      slidesPerView: 'auto'
	})
	$('#featured-movies .arrow-left').on('click', function(e){
	      e.preventDefault()
	      featuredMoviesSwiper.swipePrev()
	})
	$('#featured-movies .arrow-right').on('click', function(e){
	      e.preventDefault()
	      featuredMoviesSwiper.swipeNext()
	})


	var newlyAddedMoviesSwiper = new Swiper('#newly-added-movies .swiper-container',{
	      grabCursor: true,
	      slidesPerView: 'auto'
	})
	$('#newly-added-movies .arrow-left').on('click', function(e){
	      e.preventDefault()
	      newlyAddedMoviesSwiper.swipePrev()
	})
	$('#newly-added-movies .arrow-right').on('click', function(e){
	      e.preventDefault()
	      newlyAddedMoviesSwiper.swipeNext()
	})

	$(".movieposter").hover(function() {
	      $(this).toggleClass("show-playicon");
	});


	/*$('body').popover({
		selector: '.movieposter',
		trigger: 'hover',
		container: 'body',
		placement: 'auto',
		delay: {show: 400, hide: 0},
		animation: true,
		html: true
	});*/
});