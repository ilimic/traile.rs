<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package trailers
 */

get_header(); ?>
	<header class="frontpage">
		<a class="arrow-left" href="#"></a>
		<a class="arrow-right" href="#"></a>
		<div class="swiper-container">
			<div class="swiper-wrapper">
				<?php
				$header_slides = new WP_Query(array(
						'post_type' => 'header_slide',
						'posts_per_page' => -1,
					)
				); ?>
				<?php if ( $header_slides->have_posts() ) : ?>
					<?php while ( $header_slides->have_posts() ) : $header_slides->the_post(); ?>
						<?php
						$header_slide_meta = get_post_custom();

						$background_image_meta = get_post_meta(get_the_ID(), '_settings_background_image', true);
						$background_image = array();
						$background_image_css = '';

						if ($background_image_meta)
							$background_image = wp_get_attachment_image_src($background_image_meta, '1600x600');

						if ($background_image)
							$background_image_css = 'background-image: url('.esc_url($background_image[0]).');';
						
						$header_slide_link = '';
						if( $header_slide_meta['_links_use_link'][0] == 'movie_link' ) {
							$header_slide_link = esc_url( get_permalink( $header_slide_meta['_links_movie_link'][0] ) );
						} else {
							$header_slide_link = esc_url( $header_slide_meta['_links_custom_link'][0] );
						}
						?>
						<div class="swiper-slide">
							<div class="content-slide" style="<?php echo $background_image_css; ?>">
								<a class="background-link" href="<?php echo $header_slide_link; ?>"></a>
							</div>
						</div>
					<?php endwhile; ?>
					<!-- put pagination functions here -->
					<?php wp_reset_postdata(); ?>
				<?php else:  ?>
					<?php // no CPT found ?>
				<?php endif; ?>
			</div>
		</div>
		<div class="pagination"></div>
	</header>
	<div id="featured-movies" class="movieposterrow">
		<a class="arrow-left" href="#"><span></span></a> 
		<a class="arrow-right" href="#"><span></span></a>
		<h3><a href="#">FEATURED MOVIES &gt;</a></h3>
		<div class="swiper-container">
			<div class="swiper-wrapper">
				<?php
				$featured_movies = new WP_Query(array(
					'post_type' => 'movie',
					'posts_per_page' => 40,
					'no_found_rows' => true, // skip SQL_CALC_FOUND_ROWS, we don't need to paginate these
					'tax_query' => array(
						array(
							'taxonomy' => 'movie_tag',
							'field'    => 'slug',
							'terms'    => array( 'featured' ),
						),
					),
				)); ?>
				<?php if ( $featured_movies->have_posts() ) : ?>
					<?php while ( $featured_movies->have_posts() ) : $featured_movies->the_post(); ?>
						<?php
						$movie_poster_meta = get_post_meta(get_the_ID(), '_images_poster', true);
						$movie_poster = array();

						if ( $movie_poster_meta )
							$movie_poster = wp_get_attachment_image_src($movie_poster_meta, '220x318');

						if ( $movie_poster ) {
							?>
							<div class="swiper-slide">
								<div class="movieposter">
									<a class="posterlink" href="<?php the_permalink(); ?>"><img src="<?php echo esc_url( $movie_poster[0] ); ?>" alt="<?php echo get_the_title(); ?>"></a>
									<a class="playicon glyphicon glyphicon-play" href="<?php the_permalink(); ?>"></a>
								</div>
							</div>
							<?php
						}
						?>
					<?php endwhile; ?>
					<!-- put pagination functions here -->
					<?php wp_reset_postdata(); ?>
				<?php else:  ?>
					<?php // no CPT found ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div id="newly-added-movies" class="movieposterrow">
		<a class="arrow-left" href="#"><span></span></a> 
		<a class="arrow-right" href="#"><span></span></a>
		<h3><a href="#">NEWLY ADDED MOVIES &gt;</a></h3>
		<div class="swiper-container">
			<div class="swiper-wrapper">
				<?php
				$newly_added_movies = new WP_Query(array(
					'post_type' => 'movie',
					'posts_per_page' => 40,
					'no_found_rows' => true, // skip SQL_CALC_FOUND_ROWS, we don't need to paginate these
					'orderby' => 'date',
					'order' => 'DESC',
				)); ?>
				<?php if ( $newly_added_movies->have_posts() ) : ?>
					<?php while ( $newly_added_movies->have_posts() ) : $newly_added_movies->the_post(); ?>
						<?php
						$movie_poster_meta = get_post_meta(get_the_ID(), '_images_poster', true);
						$movie_poster = array();

						if ( $movie_poster_meta )
							$movie_poster = wp_get_attachment_image_src($movie_poster_meta, '220x318');

						if ( $movie_poster ) {
							?>
							<div class="swiper-slide">
								<div class="movieposter">
									<a class="posterlink" href="<?php the_permalink(); ?>"><img src="<?php echo esc_url( $movie_poster[0] ); ?>" alt="<?php echo get_the_title(); ?>"></a>
									<a class="playicon glyphicon glyphicon-play" href="<?php the_permalink(); ?>"></a>
								</div>
							</div>
							<?php
						}
						?>
					<?php endwhile; ?>
					<!-- put pagination functions here -->
					<?php wp_reset_postdata(); ?>
				<?php else:  ?>
					<?php // no CPT found ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
