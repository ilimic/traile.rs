<?php
/**
 * trailers functions and definitions
 *
 * @package trailers
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'trailers_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function trailers_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on trailers, use a find and replace
	 * to change 'trailers' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'trailers', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'trailers' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	add_image_size( '214x317', 214, 317, true ); // movieposter image for movieposterrow
        
        show_admin_bar( false ); // hide admin bar on frontend
	
	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'trailers_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // trailers_setup
add_action( 'after_setup_theme', 'trailers_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function trailers_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'trailers' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'trailers_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function trailers_scripts() {
	/*
	 * Global styles
	 */
	wp_enqueue_style('trailers-font-pt-sans', '//fonts.googleapis.com/css?family=PT+Sans');
	wp_enqueue_style('trailers-font-maven-pro', '//fonts.googleapis.com/css?family=Maven+Pro');
	wp_enqueue_style( 'trailers-bootstrap', get_template_directory_uri().'/css/bootstrap.min.css' );
	wp_enqueue_style( 'idangerous-swiper', get_template_directory_uri().'/css/idangerous.swiper.css' );
	wp_enqueue_style( 'trailers-main', get_template_directory_uri().'/css/main.css', array(), 20141130 );
	
	/*
	 * Global scripts
	 */
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'respond', get_template_directory_uri().'/js/respond.min.js', array(), null, false );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri().'/js/bootstrap.min.js', array(), null, false );
	wp_enqueue_script( 'idangerous-swiper', get_template_directory_uri().'/js/idangerous.swiper.min.js', array(), null, true );
	wp_enqueue_script( 'trailers-main', get_template_directory_uri().'/js/main.js', array(), 20141130, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'trailers_scripts' );

/**
 * Quick fix to display all movies on the archive page
 */
function limit_posts_per_archive_page() {
	if ( is_category() )
		set_query_var('posts_per_archive_page', 500); // or use variable key: posts_per_page
}
add_filter('pre_get_posts', 'limit_posts_per_archive_page');

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load Cuztom Wordpress Helper.
 */
require get_template_directory() . '/inc/cuztom.php';

/**
 * Load desktop navigation walker.
 */
require get_template_directory() . '/inc/walker/Trailers_Desktop_Navigation_Walker.php';