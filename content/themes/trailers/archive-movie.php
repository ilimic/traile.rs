<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package trailers
 */

get_header(); ?>
	<header class="regularpage">
		<h1>All Movies</h1>
	</header>
	<div class="movies-ct">
		<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php
				$movie_poster_meta = get_post_meta(get_the_ID(), '_images_poster', true);
				$movie_poster = array();

				if ( $movie_poster_meta )
					$movie_poster = wp_get_attachment_image_src($movie_poster_meta, '220x318');

				if ( $movie_poster ) {
					?>
					<div class="movieposter">
						<a class="posterlink" href="<?php the_permalink(); ?>"><img src="<?php echo esc_url( $movie_poster[0] ); ?>" alt="<?php echo get_the_title(); ?>"></a>
						<a class="playicon glyphicon glyphicon-play" href="<?php the_permalink(); ?>"></a>
					</div>
					<?php
				}
				?>
			<?php endwhile; ?>
		<?php else : ?>
			<p>No movies found.</p>
		<?php endif; ?>
	</div>
<?php get_footer(); ?>
