<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package trailers
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="fb-root"></div>
<script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<?php
$mainnavct_class = '';
if (is_front_page())
	$mainnavct_class = 'frontpage';
else if ( is_singular( 'movie' ) )
	$mainnavct_class = 'trailerpage';
else
	$mainnavct_class = 'regularpage';
?>
<div id="mainnavct" class="<?php echo $mainnavct_class; ?>">
	<div id="mainnav">
		<div class="gradient"></div>
		<nav class="navbar navbar-default" role="navigation">
		 

		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-links">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		      </button>
			<a class="navbar-brand" href="<?php echo get_home_url(); ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="<?php bloginfo( 'name' ); ?>">
			</a>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="navbar-links">
			<?php wp_nav_menu( array(
				'theme_location' => 'primary',
				'container' => false,
				'menu_class' => 'nav navbar-nav',
				'walker' => new Trailers_Desktop_Navigation_Walker()
			) ); ?>
			<form id="mainnav-search" class="navbar-form navbar-right" role="search">
				<div class="form-group has-feedback">
					<input class="form-control input-sm" type="text" placeholder="movies, tv shows, actors" name="s">
					<span class="glyphicon glyphicon-search form-control-feedback"></span>
				</div>
			</form>
		    </div><!-- /.navbar-collapse -->
		</nav>
	</div>
</div>
<div id="main-container" class="container-fluid">