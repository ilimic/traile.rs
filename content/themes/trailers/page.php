<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package trailers
 */

get_header(); ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<header class="regularpage">
			<?php the_title( '<h1>', '</h1>' ); ?>
		</header>
		<article>
			<?php the_content(); ?>
			<?php
				wp_link_pages( array(
					'before' => '<div class="page-links">' . __( 'Pages:', 'trailers' ),
					'after'  => '</div>',
				) );
			?>
			<?php
				// If comments are open or we have at least one comment, load up the comment template
				/*if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;*/
			?>
		</article>
	<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
