<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package trailers
 */
?>
	<footer>
		<div class="row">
			<div class="col-sm-6">
				<ul class="left">
					<li><a href="#">Traile.rs</a></li>
					<li><a href="#">Movies</a></li>
					<li><a href="#">TV Shows</a></li>
					<li><a href="#">About</a></li>
				</ul>
			</div>
			<div class="col-sm-6">
				<ul class="right">
					<li><a href="#">Terms of Use</a></li>
					<li><a href="#">Privacy Policy</a></li>
					<li>&copy; 2014 Traile.rs</li>
				</ul>
			</div>
		</div>
	</footer>
</div> <!-- end container -->

<?php wp_footer(); ?>

</body>
</html>
