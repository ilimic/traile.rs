<?php

if( ! defined( 'ABSPATH' ) ) exit;

class Cuztom_Field_Youtubeid extends Cuztom_Field
{
	var $_supports_repeatable 	= true;
	var $_supports_bundle		= true;
	var $_supports_ajax			= true;

	var $css_classes			= array( 'cuztom-input' );

	function save_value( $value )
	{
		if( is_array( $value ) )
			array_walk_recursive( $value, array( &$this, 'do_get_youtube_id' ) );
		else
			$value = $this->get_youtube_id( $value );

		return $value;
	}

	function do_get_youtube_id( &$value )
	{
		$value = $this->get_youtube_id( $value );
	}
	
	/**
	 * Get Youtube video ID from URL, eg. http://www.youtube.com/watch?v=dQw4w9WgXcQ&key=val&... => dQw4w9WgXcQ
	 * 
	 * @param string $url_or_id Youtube URL or video ID (eg. dQw4w9WgXcQ)
	 * @return string
	 */
	function get_youtube_id( $url_or_id )
	{
		if( strlen($url_or_id) == 11 )
			return $url_or_id;
		
		$vars = array();
		parse_str( parse_url( $url_or_id, PHP_URL_QUERY ), $vars );
		return isset( $vars['v'] ) ? $vars['v'] : '';
	}

	function _output( $value )
	{
		if( !empty( $value ) )
			$this->explanation = '<a target="_blank" href="http://www.youtube.com/watch?v='.esc_attr( $value ).'">http://www.youtube.com/watch?v='.esc_html( $value ).'</a>';
		return parent::_output( $value );
	}
}