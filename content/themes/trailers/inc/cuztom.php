<?php

define( 'CUZTOM_TEXTDOMAIN', 'trailers' );
define( 'CUZTOM_URL', get_template_directory_uri() . '/inc/cuztom' );

require( 'cuztom/cuztom.php' );

$header_slide = new Cuztom_Post_Type('header slide', array(
	'menu_icon' => 'dashicons-slides',
	'supports' => array('title', 'editor')
));

$movie = new Cuztom_Post_Type('movie', array(
	'menu_icon' => 'dashicons-slides',
	'supports' => array('title', 'editor')
));

$movie_genre_category = register_cuztom_taxonomy( 'Movie Genre', 'movie' );

$movie_tag_category = register_cuztom_taxonomy( 'Movie Tag', 'movie', array(
	'hierarchical' => false,
) );

if (is_admin()) {
	$header_slide
		->add_meta_box(
		'settings', 'Settings', array(
			array(
				'name' => 'background_image',
				'label' => 'Background image',
				'description' => '',
				'type' => 'image',
			),
		))
		->add_meta_box('links', 'Links', array(
			array(
				'name'          => 'use_link',
				'label'         => 'Use link',
				'description'   => 'What link to use',
				'type'          => 'select',
				'options'       => array(
					'custom_link'    => 'Custom link',
					'movie_link'    => 'Movie link',
				),
				'default_value' => 'movie_link'
			),
			array(
				'name'          => 'movie_link',
				'label'         => 'Movie link',
				'description'   => 'Movie to link to',
				'type'          => 'post_select',
				'args'          => array(
					'post_type' => 'movie',
				)
			),
			array(
				'name'          => 'custom_link',
				'label'         => 'Custom link',
				'description'   => 'Custom URL',
				'type'          => 'text',
			),
		));

	$movie
		->add_meta_box('release_dates', 'Release Dates', array(
			'bundle',
			array(
				array(
					'name' => 'release_country',
					'label' => 'Country',
					'description' => 'Country',
					'type' => 'text',
				),
				array(
					'name' => 'release_date',
					'label' => 'Release date',
					'description' => 'Release date',
					'type' => 'date',
				),
			)
		))
		->add_meta_box('ratings', 'Ratings', array(
			array(
				'name'          => 'mpaa_rating',
				'label'         => 'MPAA rating',
				'description'   => 'Motion Picture Association of America\'s (MPAA) film-rating',
				'type'          => 'select',
				'options'       => array(
					'rating_unknown'=> 'Unknown',
					'rating_notyet'	=> 'This Film Is Not Yet Rated',
					'rating_g'	=> 'G – General Audiences',
					'rating_pg'	=> 'PG – Parental Guidance Suggested',
					'rating_pg13'	=> 'PG-13 – Parents Strongly Cautioned',
					'rating_r'	=> 'R – Restricted',
					'rating_nc17'	=> 'NC-17 – Adults Only',
					'rating_nr'	=> 'Not Rated/Unrated',
				),
				'default_value' => 'rating_unknown'
			),
			array(
				'name'          => 'bbfc_rating',
				'label'         => 'BBFC rating',
				'description'   => 'The British Board of Film Classification (BBFC) film rating',
				'type'          => 'select',
				'options'       => array(
					'rating_unknown'=> 'Unknown',
					'rating_uc'	=> 'Uc - Especially suitable for pre-school children',
					'rating_u'	=> 'U (Universal) - Suitable for all',
					'rating_pg'	=> 'PG (Parental Guidance)',
					'rating_12a'	=> '12A - Cinema release suitable for 12 years and over',
					'rating_12'	=> '12 – Video release suitable for 12 years and over',
					'rating_15'	=> '15 - Suitable only for 15 years and older',
					'rating_18'	=> '18 - Suitable only for adults',
					'rating_r18'	=> 'R18 (Restricted 18) - Adult works for licensed premises only',
				),
				'default_value' => 'rating_unknown'
			),
		))
		->add_meta_box('images', 'Images', array(
			array(
				'name' => 'poster',
				'label' => 'Poster',
				'description' => '214x317 movie poster image',
				'type' => 'image',
			),
		))
		->add_meta_box('trailer', 'Trailer', array(
			array(
				'name' => 'youtube_id',
				'label' => 'Youtube URL/ID',
				'description' => 'Youtube URL or ID of the video',
				'type' => 'youtubeid',
			),
		));
}
